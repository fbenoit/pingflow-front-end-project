import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import { routes } from "./routes";
import AsyncComputed from "vue-async-computed";
import VueResource from "vue-resource";

Vue.use(AsyncComputed);
Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(VueResource);
const router = new VueRouter({ routes });

new Vue({
  el: "#app",
  router,
  components: {
  },
  data: {
    ships: []
  },
  render: h => h(App)
});
