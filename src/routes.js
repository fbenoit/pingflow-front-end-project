import Starships from "./components/Starships.vue";
import Characters from "./components/Characters.vue";
import Login from "./components/Login.vue";
import Signup from "./components/Signup.vue";
import HomePage from "./components/HomePage.vue";

export const routes = [
  { path: "/", component: HomePage },
  { path: "/starships", component: Starships },
  { path: "/characters", component: Characters },
  { path: "/signup", component: Signup },
  { path: "/login", component: Login }
];
